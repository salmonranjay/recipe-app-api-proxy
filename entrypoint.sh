#!/bin/bash

#Entrypoint for docker image

set -e # if any line fails return error to the screen immediately


# use environment subustition to pass values into template file
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf 

# start nginx service with the daemon task off so it runs in the foreground of docker container
nginx -g 'daemon off;'
